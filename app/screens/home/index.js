import * as React from 'react'
import { View } from 'react-native'
import styles from './index.styles'
import Notification from '../../components/notification'

const Home = () => (
  <View style={styles.container}>
    <Notification msg='Example notification' />
  </View>
)

export default Home
