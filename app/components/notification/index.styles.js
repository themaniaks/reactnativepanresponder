import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  notification: {
    zIndex: 99,
    padding: 20,
    minWidth: '90%',
    maxWidth: '90%',
    borderRadius: 10,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  notificationText: { fontSize: 14, lineHeight: 18 }
})

export default styles
