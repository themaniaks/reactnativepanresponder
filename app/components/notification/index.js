// @flow
import React from 'react'
import { Text, Animated, PanResponder, Dimensions } from 'react-native'
import Color from '../../styles/colors'
import styles from './index.styles'

type PropsT = {
  msg: string,
  color?: string,
  textColor?: string,
  onFinishExitAnimation?: Function
}

type StateT = {
  positionX: Animated.Value
}

const screenWidth = Dimensions.get('window').width

class Notification extends React.Component<PropsT, StateT> {
  static defaultProps = {
    color: Color.RED,
    textColor: Color.WHITE,
    onFinishExitAnimation: () => {}
  }

  state = {
    positionX: new Animated.Value(0)
  }

  animateNotification = (dx, vx) =>
    Math.abs(vx) >= 0.5 || Math.abs(dx) >= 0.5 * screenWidth
      ? Animated.timing(this.state.positionX, {
          toValue: dx > 0 ? screenWidth : -screenWidth,
          duration: 200
        }).start(() => this.props.onFinishExitAnimation())
      : Animated.spring(this.state.positionX, {
          toValue: 0,
          bounciness: 10
        }).start()

  _panResponder = PanResponder.create({
    onStartShouldSetPanResponder: (evt, gestureState) => true,
    onPanResponderMove: (event, gestureState) => {
      this.setState({ positionX: new Animated.Value(gestureState.dx) })
    },
    onPanResponderRelease: (evt, gestureState) => {
      this.animateNotification(gestureState.dx, gestureState.vx)
    }
  })

  render() {
    const { msg, color, textColor } = this.props
    return (
      <Animated.View
        {...this._panResponder.panHandlers}
        style={{
          ...styles.notification,
          backgroundColor: color,
          transform: [{ translateX: this.state.positionX }]
        }}
      >
        <Text style={[styles.text, { color: textColor }]}>{msg}</Text>
      </Animated.View>
    )
  }
}

export default Notification
