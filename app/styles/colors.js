const Color = Object.freeze({
  RED: '#FF0000',
  ORANGE: '#FFAF00',
  WHITE: '#FFFFFF'
})

export default Color
